import { Component, OnInit } from '@angular/core';
import { documenttestService } from '../documettest.service';
import { DocumentTest } from '../Documenttest';

@Component({
  selector: 'app-documents-test',
  templateUrl: './documents-test.component.html',
  styleUrls: ['./documents-test.component.css']
})

export class DocumentsTestComponent implements OnInit {

  documents: DocumentTest[];

  constructor( private service:documenttestService) { }

  ngOnInit() {
    this.indexDocuments();

  }

  indexDocuments(): void {
    this.service.indexDocuments().subscribe( res =>{
      this.documents = res;
    } )
  }


}
