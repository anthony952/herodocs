import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentsTestComponent } from './documents-test.component';

describe('DocumentsTestComponent', () => {
  let component: DocumentsTestComponent;
  let fixture: ComponentFixture<DocumentsTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentsTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentsTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
