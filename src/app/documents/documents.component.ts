import { Component, OnInit, Input,Inject } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators  } from '@angular/forms';
import { DocumentService } from '../document.service';
import { Document } from '../Document';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatTabChangeEvent } from '@angular/material';
import { Router } from "@angular/router";
import { Location } from '@angular/common';

export interface DialogData {
  id: number;

}


@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.css']
})
export class DocumentsComponent implements OnInit {

  constructor(public dialog: MatDialog, private formBuilder: FormBuilder, private documentService: DocumentService, private router: Router, private location: Location) { }
  selectedIndex=0;
  addForm: FormGroup;
  submitted = false;
  documents: Document[];
  tableColumns = ['id', 'name', 'description', 'actions'];

  ngOnInit() {



    this.getAllDocuments();



    this.addForm = this.formBuilder.group({
      // id: [],
      name: ['', Validators.required],
      description: ['', Validators.required],
      departament_id:1
    });
  }


  getAllDocuments(): void {
    this.documentService.getAllDocuments().subscribe(data=>{
      this.documents = data;
    });
  };


  onSubmit(){
    this.submitted = true;
    if(this.addForm.valid){
      this.documentService.addDocument(this.addForm.value)
      .subscribe( data => {

          this.documents = this.documents.concat(data);
          this.selectedIndex=0;
          console.log(this.selectedIndex);
      });
    }
  }


  delete(element): void{
    const dialogRef = this.dialog.open(deleteDialog, {
      width: '250px',
      data: {name: element.name}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result=="delete"){
        this.documentService.deleteDocument(element.id).subscribe(data=>{

          let newDocuments = this.documents.filter(document => document.id !== element.id);
          this.documents = newDocuments;
        })
      }
      console.log('The dialog was closed');

    });
  }
  get f() { return this.addForm.controls; }

  getDocuments = () => { return this.documents === undefined ? "" : this.documents; }

  onLinkClick(event: MatTabChangeEvent) {
    this.selectedIndex=event.index;
    console.log('event => ', event);
    console.log('index => ', event.index);
    console.log('tab => ', event.tab);
    if(event.index == 0)
    // this.router.navigate(['documents/add']);
    {this.location.go("documents")}

    if(event.index == 1)
    // this.router.navigate(['documents/add']);
    {this.location.go("documents/add")}


  }


}











@Component({
  selector: 'deleteDialog',
  templateUrl: 'deleteDialog.html',
})
export class deleteDialog {

  constructor(
    public dialogRef: MatDialogRef<deleteDialog>,
    @Inject(MAT_DIALOG_DATA) public data:DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  deleteUp() {
    this.closeDialog('delete');
  }

  closeDialog(button: 'delete') {
    this.dialogRef.close(button);
  }

}
