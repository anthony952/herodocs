import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DocumentTest } from './Documenttest';

@Injectable({
  providedIn: 'root'
})

export class documenttestService {

  uri = 'http://localhost/api/document';

  constructor( private api:HttpClient ){};

  indexDocuments(){
    return this.api.get<DocumentTest[]>(this.uri);
  }

}
