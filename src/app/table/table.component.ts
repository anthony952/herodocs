import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { ViewChild } from '@angular/core'
import { Document } from '../Document';


@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})

export class TableComponent implements OnInit{

  @Input() documents: Document[];
  @Input() displayedColumns: any;
  @Output() delete = new EventEmitter<string>();
  dataSource;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnChanges(){
    this.dataSource = new MatTableDataSource<Document>(this.documents);
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }

  editDocument(event, id): void
  {
    console.log(id);
  }

  deleteElement(event, element): void
  {
    this.delete.next(element);
  }

}
