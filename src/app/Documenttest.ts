export class DocumentTest {
  id:number;
  name:string;
  description:string;
  created_at: string;
  updated_at: string;
  departament_id: number;
}
