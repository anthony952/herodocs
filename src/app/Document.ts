export class Document{
  id: number;
  name: string;
  description: string;
  created_at: string;
  departament_is: string;
  updated_at: string;
}
