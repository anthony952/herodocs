import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DocumentsComponent } from './documents/documents.component';
import { DepartamentsComponent } from './departaments/departaments.component';
import { DocumentsTestComponent } from './documents-test/documents-test.component';

const routes: Routes = [
  { path: 'documents', component: DocumentsComponent },
  { path: 'documents/add', component: DocumentsComponent },
  { path: 'departaments', component: DepartamentsComponent },
  { path: 'documentstest', component: DocumentsTestComponent }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
