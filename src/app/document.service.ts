import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Document } from './Document';


@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  uri = 'http://localhost/api/document';

  constructor(private http: HttpClient) { }

  addDocument(document){
    return this.http.post<Document[]>(`${this.uri}`, document);
  }

  getAllDocuments(){
    return this.http.get<Document[]>(this.uri);
  }

  deleteDocument(id: number){
    return this.http.delete(this.uri + '/' + id);
  }
}
