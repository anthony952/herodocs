import { BrowserModule } from '@angular/platform-browser';
import { NgModule  } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TableComponent } from './table/table.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidebarComponent } from './sidebar/sidebar.component';
import {
  MatNativeDateModule
} from '@angular/material';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {MatButtonModule} from '@angular/material/button';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { DocumentsComponent } from './documents/documents.component';
import { DepartamentsComponent } from './departaments/departaments.component';
import {MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material';
import { DocumentService } from './document.service';
import {MatMenuModule} from '@angular/material/menu';
import {MatDialogModule} from '@angular/material/dialog';
import {deleteDialog} from './documents/documents.component';
import { DocumentsTestComponent } from './documents-test/documents-test.component';



@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    SidebarComponent,
    DocumentsComponent,
    DepartamentsComponent,
    deleteDialog,
    DocumentsTestComponent
  ],
  imports: [
    MatDialogModule,
    MatMenuModule,
    HttpClientModule,
    MatPaginatorModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    HttpClientModule,
    MatIconModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatListModule,
    MatButtonModule,
    MatTableModule,
    MatInputModule,
    MatTabsModule,
    MatCardModule,
    // RouterModule.forRoot(
    //   appRoutes,
    //   //{ enableTracing: true } // <-- debugging purposes only
    // )
  ],
  providers: [DocumentService],
  bootstrap: [AppComponent],
  entryComponents: [deleteDialog]
})
export class AppModule { }
